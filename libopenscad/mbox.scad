// BOF

// NOSTL






module chamfered_box(dim = [0, 0, 0], align = [0, 0, 0], chamfer = 0, center = false) {
    _chamfer = chamfer;
    falign = center ? [0, 0, 0] : align;


    translate([(dim.x / 2) * falign.x, (dim.y / 2) * falign.y, (dim.z / 2) * falign.z]) {
        hull() {
            cube([dim.x                 , dim.y - (_chamfer * 2), dim.z - (_chamfer * 2)], center = true);
            cube([dim.x - (_chamfer * 2), dim.y                 , dim.z - (_chamfer * 2)], center = true);
            cube([dim.x - (_chamfer * 2), dim.y - (_chamfer * 2), dim.z                 ], center = true);
        }

    }
}




module chamfered_wall(dim = [0, 0, 0], align = [0, 0, 0], chamfer = 0, center = false, manifold_overlap = true) {

    // only one flat edge at a time as of 2021.07.17

    _chamfer = chamfer;
    falign = center ? [0, 0, 0] : align;
    overlap = manifold_overlap ? 0.01 : 0.00;

    translate([(dim.x / 2) * falign.x, (dim.y / 2) * falign.y, (dim.z / 2) * falign.z]) {
        hull() {
            if(dim.x > 0) {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            } else {
                cube([-1 * (dim.x + overlap)          , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([-1 * (dim.x + overlap)          , dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([-1 * (dim.x + overlap)          , dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            }

            if(dim.y > 0) {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            } else {
                cube([dim.x + overlap                 , -1 * (dim.y + overlap)                 , dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), -1 * (dim.y + overlap)                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), -1 * (dim.y + overlap)                 , dim.z + overlap                 ], center = true); // full Z
            }

            if(dim.z > 0) {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), dim.z + overlap - (_chamfer * 2)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , dim.z + overlap - (_chamfer * 2)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), dim.z + overlap                 ], center = true); // full Z
            } else {
                cube([dim.x + overlap                 , dim.y + overlap - (_chamfer * 2), -1 * (dim.z + overlap)], center = true); // full X
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap                 , -1 * (dim.z + overlap)], center = true); // full Y
                cube([dim.x + overlap - (_chamfer * 2), dim.y + overlap - (_chamfer * 2), -1 * (dim.z + overlap)], center = true); // full Z
            }

        }

    }
}




module radiused_box(inside_height = 50, inside_length = 40, inside_width = 30, inside_radius = 5, wall_thickness = 10, color = "pink", chamfer = -1, align = [0, 0, 0], center = false) {
    cchamfer = (chamfer == -1) ? wall_thickness / 4 : chamfer;
    falign = center ? [0, 0, 0] : align;
    translate([
                (inside_width  / 2 + wall_thickness) * falign.x,
                (inside_length / 2 + wall_thickness) * falign.y,
                inside_height / 2 * falign.z
              ]) {

        color(color) {
            for(x = [-1 , 1]) {
                translate([x * inside_width / 2  - (x * wall_thickness), 0, 0]) {
                    chamfered_wall(dim = [wall_thickness, -(inside_length - (inside_radius - manifold_overlap) * 2), inside_height], align = [x, 0, 0], chamfer = cchamfer);
                }
            }
            for(y = [-1 , 1]) {
                translate([0, y * inside_length / 2, 0]) {
                    chamfered_wall(dim = [-(inside_width - (inside_radius + wall_thickness - manifold_overlap) * 2), wall_thickness, inside_height], align = [0, y, 0], chamfer = cchamfer);
                }
            }
        }


        color("lightgreen") {

            translate([inside_width/2 - inside_radius - wall_thickness, inside_length/2 - inside_radius, 0]) {
                rotate_extrude(angle = 90) {
                    translate([inside_radius + wall_thickness/2, 0, 0]) {
                        hull() {
                            square([wall_thickness - cchamfer * 2, inside_height], center = true);
                            square([wall_thickness, inside_height - cchamfer * 2], center = true);
                        }
                    }
                }
            }


            translate([-1 * (inside_width/2 - inside_radius - wall_thickness), inside_length/2 - inside_radius, 0]) {
                rotate([0, 0, 90]) {
                    rotate_extrude(angle = 90) {
                        translate([inside_radius + wall_thickness/2, 0, 0]) {
                            hull() {
                                square([wall_thickness - cchamfer * 2, inside_height], center = true);
                                square([wall_thickness, inside_height - cchamfer * 2], center = true);
                            }
                        }
                    }
                }
            }








            translate([inside_width/2 - inside_radius - wall_thickness, -1 * (inside_length/2 - inside_radius), 0]) {
                rotate([0, 0, 270]) {
                    rotate_extrude(angle = 90) {
                        translate([inside_radius + wall_thickness/2, 0, 0]) {
                            hull() {
                                square([wall_thickness - cchamfer * 2, inside_height], center = true);
                                square([wall_thickness, inside_height - cchamfer * 2], center = true);
                            }
                        }
                    }
                }
            }



            translate([-1 * (inside_width/2 - inside_radius - wall_thickness), -1 * (inside_length/2 - inside_radius), 0]) {
                rotate([0, 0, 180]) {
                    rotate_extrude(angle = 90) {
                        translate([inside_radius + wall_thickness/2, 0, 0]) {
                            hull() {
                                square([wall_thickness - cchamfer * 2, inside_height], center = true);
                                square([wall_thickness, inside_height - cchamfer * 2], center = true);
                            }
                        }
                    }
                }
            }



        }

    }




}



if(!is_undef(debug)) {

    $fn = 150;
    radiused_box();




    // box test in the LR quadrant
    //
    translate([20, -50, 0]) {
        chamfered_box(dim = [20, 20, 20], chamfer = 1);

        translate([30, 30, 0]) {
            chamfered_box(dim = [10, 20, 30], chamfer = 4);
        }
    }

    translate([50, -70, 0]) {
         chamfered_wall(dim = [30, 10, 20], align = [0, 0, 0], chamfer = 2, center = true, manifold_overlap = true);

    }

}

// EOF
